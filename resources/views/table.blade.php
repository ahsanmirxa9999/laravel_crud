<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400&display=swap" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="./assets/css/bootstrap.min.css">

    <!-- Style -->
    <link href="./assets/css/main.css" rel="stylesheet" media="all">

    <title>Laravel Table</title>
</head>

<body>
    <div class="content">
        <div class="container">
            <div class="row row-space">
                <h2 class="mb-5">Laravel Table</h2>
                <div class="col-2">
                    <a class="btn btn-success" href="{{ url('/') }}" role="button">Add Table</a>
                </div>
            </div>
                    <div class="table-responsive">
                        <table class="table caption-top">
                            <thead>
                                <tr>
                                    <th scope="col">Id</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Phone</th>
                                    <th scope="col">Birthday</th>
                                    <th scope="col">Gender</th>
                                    <th scope="col">Image</th>
                                    <th scope="col">Update</th>
                                    <th scope="col">Delete</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($data as $form)
                                    <tr>
                                        <th scope="row">{{ $form->id }}</th>
                                        <td>{{ $form->name }}</td>
                                        <td>{{ $form->email }}</td>
                                        <td>{{ $form->phone }}</td>
                                        <td>{{ $form->birthday }}</td>
                                        <td>{{ $form->gender }}</td>
                                        @php
                                            $image = explode(',', $form->images);
                                        @endphp
                                        @foreach ($image as $images)
                                            <td><img src="/laravel_img/{{ $images }}" alt="" class="img-responsive" style="max-height:60px; max-width:50px"></td>
                                        @endforeach
                                        <td><a href="{{ '/view/edit' }}/{{ $form->id }}" class="btn btn-success">Update</a></td>
                                        <td><a href="{{ url('delete', $form->id) }}" class="btn btn-danger">Delete</a></td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
</body>

</html>

<script>
      // ============== Delete Toastr ==================
  @if(Session::has('danger'))
      toastr.options = {
          "closeButton":true,
          "progressBar":true
      }
      toastr.error("{{ session('danger') }}",'error!',{timeOut:2000});
  @endif

    // ============== Update toastr =============
  @if(Session::has('success'))
      toastr.options = {
          "closeButton":true,
          "progressBar":true
      }
      toastr.success("{{ session('success') }}",'success!',{timeOut:2000});
  @endif
</script>
