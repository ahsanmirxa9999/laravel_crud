<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Form;

class FormController extends Controller
{
    public function index(){
        return view('form');
    }

    public function add_data(Request $request){
        $data = new Form;
        $data->name=$request->input('name');
        $data->email=$request->input('email');
        $data->phone=$request->input('phone');
        $data->birthday=$request->input('birthday');
        $data->gender=$request->input('gender');
        $data->password=$request->input('password');
        $image=[];
        if($request->images){
            foreach($request->images as $key => $img){
                $extension = $img->getClientOriginalExtension();
                $filename =Rand(1000, 9000).'.'.$extension;
                $img->move(public_path('/laravel_img'),$filename);
                $image[]=$filename;
            }
        }
        $myimage=implode(',',$image);
        $data->images=$myimage;
        $data->save();
        return redirect()->back()->with('success','Form has been inserte Successfully');
    }

    // ================= view query ================

    public function table(){
        $data =Form::all();
        return view('table')->with(compact('data'));
    }

    // ================ Delete Query ================

    public function delete($id){
        $data = Form::find($id);
        $imgs=explode(',',$data->images);
        foreach($imgs as $images){
            \File::delete('laravel_img/'.$images);
        }
            $data->delete();
            return redirect()->back()->with('danger',"Form has been Deleted");
    }

    // ================ Update Query =================
    public function edit($id){
        $data = Form::find($id);
        return view('update')->with(compact('data'));
    }

    public function update(Request $request, $id){
        $data = Form::find($id);
        $data->name=$request['name'];
        $data->email=$request['email'];
        $data->phone=$request['phone'];
        $data->birthday=$request['birthday'];
        $data->gender=$request['gender'];
        $data->password=$request['password'];
        $image=[];
        if($request->images){
            foreach($request->file('images') as $imagefile){
                $extension = $imagefile->getClientOriginalExtension();
                $filename =Rand(1000, 9000).'.'.$extension;
                $imagefile->move(public_path('/laravel_img'),$filename);
                $image[]=$filename;
            }
        }
        $myimage=implode(',',$image);
        $data->images=$myimage;
        $data->save();
        return redirect('/table')->with('success','Form has been Updated Successfully');
    }

}
