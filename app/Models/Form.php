<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Form extends Model
{
    use HasFactory;
    protected $table = '_form';
    protected $id = 'id';
    protected $fillable = [
        'name',
        'email',
        'phone',
        'birthday',
        'gender',
        'password',
    ];
}
