<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\FormController;

Route::get('/',[FormController::class,'index']);
Route::post('/add_data',[FormController::class,'add_data']);
Route::get('/table',[FormController::class,'table']);
Route::get('/delete/{id}', [FormController::class, 'delete']);
Route::get('/view/edit/{id}', [FormController::class, 'edit']);
Route::post('/update/{id}', [FormController::class, 'update']);
